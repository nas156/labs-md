import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const GeneralScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.mainText}>Гевеленко Назар</Text>
            <Text style={styles.secondaryText}>Група ІВ-82</Text>
            <Text style={styles.secondaryText}>ЗК ІВ-8205</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainText: {
        fontWeight: "bold",
        fontSize: 20
    },
    secondaryText: {
        fontSize: 15
    }
});

export default GeneralScreen;
