import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import GeneralScreen from "../screens/GeneralScreen";
import Item2Screen from "../screens/Item2Screen";
import  MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Tab = createBottomTabNavigator();

const MyTabs = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'General') {
                    iconName = focused
                        ? 'favorite'
                        : 'favorite-border';
                } else if (route.name === 'Item2') {
                    iconName = focused ? 'lightbulb' : 'lightbulb-outline';
                }

                return <MaterialIcons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'black',
        }}
    >
        <Tab.Screen name="General" component={GeneralScreen}/>
        <Tab.Screen name="Item2" component={Item2Screen}/>
    </Tab.Navigator>
);

export default MyTabs;
